## Project to run templates for freestyle


    - sbt new jmjava/freestyle-lib-template.g8 --name=freestylelib --force
    - cd freestylelib/
    - sbt publishLocal
    - cd ..
    - sbt new jmjava/freestyle-rpc-runtime-template.g8 --name=freestyleruntime --force
    - cd freestyleruntime/
    - sbt publishLocal
    - cd ..
    - sbt new jmjava/freestyle-rpc-protocol-template.g8 --name=freestyleprotocol --force
    - cd freestyleprotocol
    - sbt publishLocal
    - cd ..
    - sbt new jmjava/freestyle-rpc-client-template.g8 --name=freestyleclient --force
    - sbt clean compile
    - cd ..
    - sbt new jmjava/freestyle-rpc-server-template.g8 --name=freestyleserver --force
    - sbt clean compile
